# run-gwas

__version__: `0.1.1a0`

This project aims to put together a bunch of Python code and bash scripts to work towards a loose pipeline to run GWAS and lots of the common post-GWAS tasks. It will likely change over time.

There is an installable Python package but lots of the heavy lifting will be performed by bash scripts.

There is [online](https://cfinan.gitlab.io/run-gwas/) documentation for run-gwas.

## Installation instructions
At present, run-gwas is undergoing development and no packages exist yet on PyPi. Therefore it is recommended that you install via conda unless you have access to gwas-norm, in which case you can use a pip install from the git repository.

### Installation using conda
I maintain a conda package in my personal conda channel. To install from this please run:

```
conda install -c cfin -c bioconda -c conda-forge run-gwas
```

There are currently builds for Python v3.8, v3.9 for Linux-64 and Mac-osX, currently v3.10 is not supported directly as there is no py310 conda build for bgen-reader.  If one does not get updated in the near future, I will package one up. If you want to use it on Python v3.10, I recommend you install via pip as detailed below.

Please keep in mind that all development is carried out on Linux-64 and Python v3.8/v3.9. I do not own a Mac so can't test on one, the conda build does run some import tests but that is it.

### Installation using pip
You can install using pip from the root of the cloned repository, first clone and cd into the repository root:

```
git clone git@gitlab.com:cfinan/run-gwas.git
cd run-gwas
```

Install the dependencies:
```
python -m pip install --upgrade -r requirements.txt
```

Then install using pip
```
python -m pip install .
```

Or for an editable (developer) install run the command below from the root of the repository. The difference with this is that you can just to a `git pull` to update, or switch branches without re-installing:
```
python -m pip install -e .
```

### Conda dependencies
There are also conda yaml environment files in `./resources/conda/envs` that have the same contents as `requirements.txt` but for conda packages, so all the pre-requisites. I use this to install all the requirements via conda and then install the package as an editable pip install.

However, if you find these useful then please use them. There are Conda environments for Python v3.8, v3.9, v3.10.

### Required repositories not in pypy or conda
All of the methods listed below will require the installation of the [hpc-tools](https://gitlab.com/cfinan/cluster) package.

### Required non-python repositories
You will also need to install [bash-helpers](https://gitlab.com/cfinan/bash-helpers) and have them sourceable in your path.

### Adding the bash GWAS tasks to your path
The python installation methods below will not install the associated bash scripts into your path, you will need to do that manually. After cloning this repository add `./resources/bin` to your `$PATH` in your `~/.bashrc` or `~/.bash_profile` (which ever you use), where `.` is the root of this repository. For example, if you keep all of your clone repositories in a directory called `my_code` then:

```
PATH="${PATH}:${HOME}/my_code/run-gwas/resources/bin"
export PATH
```

Don't forget to open a new terminal or source your `~/.bashrc` or `~/.bash_profile` after you have modified it so the changes will take effect.

### Installation of third party programs
Currently, the pipeline just acts as an easy to use interface to run 3rd party GWAS applications in parallel on an HPC. Therefore, it requires these programs to be installed __and available in your path__. These are listed below.

* [sqlite3](https://www.sqlite.org/index.html)
* [bgenix](https://enkre.net/cgi-bin/code/bgen/wiki/bgenix)
* [bgenie](https://jmarchini.org/bgenie/)
