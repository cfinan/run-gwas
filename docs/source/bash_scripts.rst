==========================
``./resouces/bin`` scripts
==========================

Alongside the installed Python scripts, there are several BASH scripts that may be of use. You can only access these if you clone the ``run-gwas`` repository. To install these you will need the ``run-gwas/resources/bin`` directory within your ``$PATH`` in your ``~/.bashrc``. For example:

.. code-block:: bash

   export "$PATH:/<your>/<path>/<to>/run-gwas/resources/bin"

Also, the `bash-helpers <https://gitlab.com/cfinan/bash-helpers>`_ repository will be installed.
Some of these are also called by :ref:`task scripts <task_scripts>` in this repository.

vcf-to-bgen.sh
--------------

This converts a VCF file into a BGEN file and optionally allows the user to subset on specific samples, genomic regions or imputation info scores prior to conversion.

This script requires that the following programs are in your ``PATH`` in your ``~/.bashrc`` or ``~/.bash_profile``:

* ``bcftools`` - https://samtools.github.io/bcftools/bcftools.html
* ``qctool`` (version 2) - https://www.well.ox.ac.uk/~gav/qctool_v2/
* ``bgenix`` - https://enkre.net/cgi-bin/code/bgen/wiki?name=bgenix

The help for ``vcf-to-bgen.sh`` is shown below:

.. code-block::

   === vcf-to-bgen.sh ===
   Convert a VCF file into a BGEN file.
   USAGE: vcf-to-bgen.sh [flags] <vcf input file> <bgen output file> [optional region1 [optional region2 [...]]]

   Convert a VCF file to a BGEN file allowing for subsetting of samples and/or extraction of one or more specific chromosomal regions prior to conversion.
   flags:
   -v,--[no]verbose:  give more output (default: false)
   -k,--[no]keep-vcf:  keep any intermediate filtered vcf files (default: false)
   -u,--[no]uni-id:  Create universal identifiers (default: false)
   -T,--tmp-dir:  An alternative location for tmp files, default is to use the system temp dir (default: '')
   -s,--samples:  a file containing a list of sample IDs (one per line) to subset into the output BGEN, if not supplied then no sample subset is performed (default: '-')
   -i,--info:  if > 0, then filter the input VCF on an INFO/R2 field of >= info (default: 0)
   -r,--info-field:  The name of the imputation info field within the INFO field of the VCF file (default: 'R2')
   -g,--geno-tag:  The name of the genotype format to use in the VCF file, either GP or GT (default: 'GP')
   -h,--help:  show this help (default: false)
