.. run-gwas documentation master file, created by
   sphinx-quickstart on Thu Dec 24 09:21:27 2020.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Contents
========

.. toctree::
   :maxdepth: 2
   :caption: Setup

   getting_started

.. toctree::
   :maxdepth: 2
   :caption: Programmer reference

   python_scripts
   bash_scripts
   task_scripts


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
