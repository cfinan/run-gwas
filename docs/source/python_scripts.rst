==============
Python scripts
==============

Below is a list of all the Python scripts that are installed with the package.

``make-genomic-chunks``
-----------------------

Parse genomic coordinates from an input file and generate coordinate boundaries of regions that contain a target number of sites (rows) in the input file. This is designed so that the region boundaries can be tabix (or bgenix) queried out to give subsets of the input file with defined size. The number of sites in a region may be greater than the target in instances where the target number of sites occurs in a region with multiple sites having the same co-ordinates or when the length of a reference allele exceeds a boundary site. This is to ensure that the end coordinate of one region is different from the start coordinate of the subsequent region and that each range query should return a distinct set of variants. The input file must be sorted on the ``chr_name``, ``start_pos`` and ``end_pos`` columns.

.. argparse::
   :module: run_gwas.genome_chunks
   :func: _init_cmd_args
   :prog: make-genomic-chunks

Output file
~~~~~~~~~~~

The script will output the following columns in all cases:

1. ``rowidx`` - The row number (sequential count indexed from 1)
2. ``region_idx`` - The region number (sequential count indexed from 1)
3. ``chr_name`` - The chromosome name
4. ``start_pos`` - The start position of the region
5. ``end_pos`` - The end position of the region
6. ``nsites`` - The number of sites present in the region

If input is from a file rather than ``<STDIN>``, then additional columns will be added:

7. ``infile`` - The input file name given to ``make-genome-chunks``
8. ``outfile`` - A potential output file name based on the input file name and the data in columns 2-5. it has the structure: ``<root infile name>.<region_idx>.<chr_name>.<start_pos>-<end_pos>.<extension>``. If the --out-dir is not specified (see below) then this path will be a basename. If the ``--out-dir`` is the keyword ``INFILE`` then the outfile will be referenced with respect to the directory of the input file. However, if ``--out-dir`` is any other directory name then the output file name is referenced with respect to that. If ``--out-ext`` is set, then it is used as the output file extension, otherwise the input file extension is used.
