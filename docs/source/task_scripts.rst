=============================
``./resources/tasks`` scripts
=============================

.. _task_scripts:

Task scripts are special BASH scripts that are designed to be used with the hpc-tools package. They are responsible for running a single task of a job array on an HPC cluster. For more information please see `hpc-tools <https://gitlab.com/cfinan/cluster>`_. In run-gwas, the task scripts are located in ``run-gwas/resources/tasks``. They are not required to be in your ``PATH``. 

All task scripts accept the same arguments, with the first 3 being required and the last one being provided by grid engine at run time or the user (while debugging). In general, you should not need to touch this script directly but you might want to run a task for debugging purposes.

1. The job array file. The job array file should have a row for task that is required to be run in parallel. The job array file should have a header and the first column should be called ``rowidx``.
2. The temp location. This is the location where you want all the intermediate files to be written to when the task is running. A working directory for each task will be created in here with the format ``run_<username>_<rowidx>_XXXXXXXX`` where ``XXXXXXXX`` are random letters and numbers used by ``mktemp``. An example argument is ``~/Scratch/tmp``.
3. The step size. You can assign this value using the ``--step`` size parameter to ``pyjasub``. This indicates that each running task should run ``--step`` number of rows from you job array file.
4. The run IDX. This corresponds to the row number that you want to run from your job array file, or if ``--step`` is > 1. The row number that you will start from. So the value of ``1`` will run the first row (after the header) from your job array file. If this is not supplied, as is the case when the task is actually running on the cluster, then ``$SGE_TASK_ID`` is used to supply the run IDX.

Debugging a specific task script/job task
-----------------------------------------

If you are having issues with any of the task scripts or job tasks then it is far easier to debug and isolate a single task interactively than debugging against the cluster. Additionally, you can also use this approach for estimating time/memory requirements for you full job. Either way, you can directly run the task script and provide the arguments it expects. For example, if the task script was called ``my_task.task.sh`` and you wanted to debug the 3 row in your job array, you would run

.. code-block::

   my_task.task.sh /path/to/job/array.ja /tmp 1 3

To run rows 3 and 4, you can increase the step size to 2.

.. code-block::

   my_task.task.sh /path/to/job/array.ja /tmp 2 3


If you want to benchmark for your specific application you can use ``/usr/bin/time -v``:

.. code-block::

   /usr/bin/time -v my_task.task.sh /path/to/job/array.ja /tmp 2 3


``bgenie.task.sh``
~~~~~~~~~~~~~~~~~~

Controls the running of bgenie GWAS analysis on a predefined chunk of the genome. This script will ensure that all your samples in phenotype and covariate files are aligned perfectly with the BGEN file that you are using. It assumes that ``bgenie``, ``bgenix`` and ``sqlite3`` are in your path (this is checked) and the missing value for ``bgenie`` is ``-999``. It runs bgenie with the ``--pval`` option on and with the default chunk size of ``256`` and a single thread ``--thread 1``.

In tests, it takes about ~15 mins to run ``bgenie`` for 10,000 variants across ~340,000 samples and this uses about ~4G RAM.

The job array file should have a row for each genomic chunk that is required to be run in parallel. The job array file is required to have these columns present (in the listed order):

1. ``rowidx`` - An incremental counter for each row after the header line. The counter starts at 1and counter should be unique for each row.
2. ``region_idx`` - An incremental  counter for the genomic region. In many cases this will be identical to the ``rowidx``, although, if the same genomic chunk is being passed through to different chunks then this may be repeated. So where as the ``rowidx`` should be unique the ``region_idx`` does not necessarily need to be unique.
3. ``chr_name`` - The chromosome name of the genomic chunk (listed exactly as it occurs in the bgen file).
4. ``start_pos`` - The start position for the genomic chunk.
5. ``end_pos`` - The end position for the genomic chunk.
6. ``nsites`` - The number of variants in the genomic chunk. This is checked against the results output to make sure that all the variants have been output ok. If not then the task has deemed to fail.
7. ``infile`` - The input bgen file (absolute path), this must have a .bgi index file associaed with in with the same path and name but with an additional .bgi file extension.
8. ``outfile`` - The output results file (absolute path)
9. ``pheno_file`` - A file containing all the phenotypes that you want to run. This must be tab delimited and have a header on the first row with the sample IDs in the first column. This will be normalised to the samples/sample order within the bgen file while the task is running.
10. ``covariate_file`` - A file containing all the covariates you want to apply in the analysis. This must be tab delimited and have a header on the first row with the sample IDs in the first column. This will be normalised to the samples/sample order within the bgen file while the task is running. If you do not want to use any covariates then supply a `-` (single hyphen (dash)) in this column. 

``vcf-to-bgen.task.sh``
~~~~~~~~~~~~~~~~~~~~~~~

This converts a VCF file into a BGEN file and optionally allows the user to subset on specific samples, genomic regions or imputation info scores prior to conversion. This calls the ``vcf-to-bgen.sh`` bash script internally, see the :ref:`documentation <vcf_to_bgen>` for more details.

The job array file should have a row for each file that is being converted (typically a chromosome) and the following columns (in the order provided).

1. ``rowidx`` - An incremental counter for each row after the header line. The counter starts at 1and counter should be unique for each row.
2. ``vcf_input`` - The path to the VCF input file that is being converted to a BGEN.
3. ``bgen_output`` - The path to the output bgen file.
4. ``keep_vcf`` - Should any intermediate filtered VCF files be kept? Should be 0 if no, 1 if yes.
5. ``uni_id`` - Do you want to create new universal identifiers of chr:pos_<alleleles in sort order>. There is an assumption that the alleles are bi-allelic. Set to 0 for no 1 if yes.
6. ``info_to_subset`` - The info value to subset on 0 means no subset > 0 means subset at that value.
7. ``info_field`` - The info field to use if info subsetting, the column needs to be there but only relevant if ``info_to_subset`` > 0. ``R2`` is a good default value.
8. ``sample_file`` - Path to a file with sample IDs to subset (one per line) or ``-`` for no sample subset.
9. ``geno_tag`` - The genotype flag to incorporate into the BGEN file GP (genotype probabilities) or GT (hard coded genotypes)
10. ``regions`` - A pipe ``|`` separated list of region boundaries to extract from the VCF file prior to BGEN convertion. See the bcftools [documentation](http://www.htslib.org/doc/1.1/bcftools.html), specifically the ``-r``, ``--regions`` parameters.

To give an indication of requirements for this task. A VCF file with ~3650 samples and ~270,000 variants, with filtering for INFO at R2>=0.3, takes about 61 mins to run and requires 10 MB of RAM.

``fastgwa-lr-combined.task.sh``
-------------------------------

Run a regular linear model GWAS with fastGWA for a bunch of bgen files at once.

``fastgwa-lr.task.sh``
----------------------

Run a regular linear model GWAS with fastGWA for a single bgen file at once. This is idea for paralleling over chromosomes.
