#!/bin/bash
USAGE=$(cat <<EOF
= vcf-to-bgen =
Convert a VCF file into a BGEN file.
USAGE: $(basename $0) [flags] <vcf input file> <bgen output file> [optional region1 [optional region2 [...]]]

Convert a VCF file to a BGEN file allowing for subsetting of samples and/or extraction of one or more specific chromosomal regions prior to conversion.
EOF
)

. shflags

# configure shflags
DEFINE_boolean 'verbose' false 'give more output' 'v'
DEFINE_boolean 'keep-vcf' false 'keep any intermediate filtered vcf files' 'k'
# Make the IDs of the final BGEN file chr_pos_<alleles in sort order>
DEFINE_boolean 'uni-id' false 'Create universal identifiers' 'u'
# Is also passed through to UNIX sort
DEFINE_string 'tmp-dir' "" 'An alternative location for tmp files, default is to use the system temp dir' 'T'
# Any samples that need to be subset from the bgen file
DEFINE_string 'samples' "-" 'a file containing a list of sample IDs (one per line) to subset into the output BGEN, if not supplied then no sample subset is performed' 's'

DEFINE_float 'info' 0 'if > 0, then filter the input VCF on an INFO/R2 field of >= info' 'i'
DEFINE_string 'info-field' "R2" 'The name of the imputation info field within the INFO field of the VCF file' 'r'
DEFINE_string 'geno-tag' "GP" 'The name of the genotype format to use in the VCF file, either GP or GT' 'g'

FLAGS_HELP="$USAGE"

# parse the command-line
FLAGS "$@" || exit $?
eval set -- "${FLAGS_ARGV}"

# The positional arguments are a list of files that want concatenating
# Run the initialisation, this creates a bunch of functions some globals that
# are useful for general tasks that the script want to perform
# *** IMPORTANT *** - This must be done after shflags has finished
VERBOSE_TO_STDERR=0
. bh_init.sh
. bh_bio.sh

# The -O parameter in bcftools
# u is uncompressed BCF the quickest (but big)
# v is compressed VCF, the slowest but small
BCF_OUT_OPT="z"

echo_prog_name "$0"
set_verbose_arg
check_at_least_positional_args 2 "$@"

# Make sure the programs that we need to use are present
info_msg "checking bcftools is in your PATH (error at this point means no)"
bcftools --help >/dev/null 2>&1
info_msg "checking qctool is in your PATH (error at this point means no)"
qctool -help >/dev/null 2>&1
info_msg "checking bgenix is in your PATH (error at this point means no)"
bgenix -help >/dev/null 2>&1

# info_msg respects the verbosity that will be defined in FLAGS_verbose
# if shflags is being used. If not set the global VERBOSE argument
info_msg "temp directory: $TMPDIR"

# The working directory is a temp directory within TMPDIR created by
# bh_init.sh and deleted upon exit (either clean or error)
info_msg "working temp directory: $WORKING_DIR"

# Grab the output file and then remove it from $@
input_vcf="$1"
info_msg "input file: $input_vcf"
shift
check_file_readable "$input_vcf" "vcf input not readable"

output_bgen="$1"
info_msg "output file: $output_bgen"
shift
check_file_writable "$output_bgen" "bgen output location not writable"

# Get the output directory as we may need it if we are keeping the
# VCF file
outdir="$(basename "$output_bgen")"

# Grab any regions - this may be empty
regions=("$@")

# Output the region filters
unset_run_env
for i in "${regions[@]}"; do
    info_msg "region filter: $i"
done
set_run_env

# Count the number of variants that we have
info_msg "counting variants..."
nvars=$(zcat "$input_vcf" | cut -f1 | grep -v "^#"  | wc -l)

# Get the initial sample count
info_msg "counting samples..."
initial_samples=$(bcftools query -l "$input_vcf" | wc -l)

working_file="$input_vcf"
run_filter=1

# If the user has supplied a sample file then we perform some checks and
# set up args for filtering on samples
if [[ "$FLAGS_samples" != '-' ]]; then
    info_msg "subsetting samples: $sample_subset_count"
    run_filter=0
    check_file_readable "$FLAGS_samples" "sample subset file not readable"
    sample_subset_count="$(cat $FLAGS_samples | wc -l)"

    # Make sure the number of samples is > 0
    if [[ $sample_subset_count -eq 0 ]]; then
        echo "ERROR: '$FLAGS_samples' contains no samples" 1>&2
        error_exit "$LINENO"
    fi

    # build the arguments for bcftools
    bcftools_args=(
        -S "$FLAGS_samples"
    )
else
    info_msg "not subsetting samples"
fi

# If the user is subsetting regions then ad them to the bcftools
# arguments
if [[ ${#regions[@]} -gt 0 ]]; then
    run_filter=0
    bcftools_args+=(
        -r "${regions[@]}"
    )
fi

# Do we want to filter on a specific info level
if [[ $FLAGS_info != "0" ]]; then
    run_filter=0
    info_filter="INFO/$FLAGS_info_field >= $FLAGS_info"
    info_msg "appling info filter: $info_filter"

    bcftools_args+=(
        -i "$info_filter"
    )
fi

# If we care filtering on regions and/or samples then run the
# filter, we will use the file that has been generated instead
# the input file.
if [[ $run_filter -eq 0 ]]; then
    info_msg "running filters"
    temp_filter_vcf="$WORKING_DIR"/$(basename "$output_bgen" | sed 's/\.bgen$/.vcf.gz/')
    bcftools view "${bcftools_args[@]}" \
             -O"$BCF_OUT_OPT" "$input_vcf" > "$temp_filter_vcf"
    TEMPFILES+=("$temp_filter_vcf")
    working_file="$temp_filter_vcf"
fi

# We use bcftools to convert to gen file first then QCtool2 to
# convert to bgen
temp_gen="$(mktemp -p"$WORKING_DIR")"
temp_gen_prefix="$(basename "$temp_gen")"

# Now we want to convert the VCF file to a GEN file
# We use the user supplied field for the genotypes
info_msg "converting to gen file..."
bcftools convert --tag "$FLAGS_geno_tag" --3N6 -g "$temp_gen" "$working_file"

# If we have run the filters (eiher sample or regions) but we do not want to
# keep the VCF file then we can remove at this stage to save some temp space
if [[ $FLAGS_keep_vcf -eq 1 ]] && [[ $run_filter -eq 0 ]]; then
    rm "$temp_filter_vcf"
fi

# bcftools will add the suffix onto the GEN data file and samples file
temp_gen_data="$temp_gen"".gen.gz"
temp_gen_sample="$temp_gen"".samples"

# If we are adding universal IDs to the variants
if [[ $FLAGS_uni_id -eq 0 ]]; then
    temp_gen_reformat="$(mktemp -p"$WORKING_DIR")"
    temp_gen_reformat="$temp_gen_reformat".gen.gz
    info_msg "generating universal IDs..."

    # Now change the IDs to CHR:POS_A1_A2 in sort order
    # This has to happen AFTER gen conversion as BCF tools
    # automatically does CHR:POS_REF_ALT
    zcat "$temp_gen_data" |
        awk '{split($2,a,":"); split($5":"$6, b, ":"); asort(b,c); newid=a[1]":"$4"_"c[1]"_"c[2]; $2=newid; $3=newid; print $0}' |
        bgzip > "$temp_gen_reformat"
    # Remove the non ID'd file as we do not need it anymore
    rm "$temp_gen_data"
    temp_gen_data="$temp_gen_reformat"
fi

# Get a count of the variants in GEN file
info_msg "counting gen file variants..."
nvar_gen=$(zcat "$temp_gen_data" | wc -l)

# temp files for creating the BGEN
temp_bgen="$(mktemp -p"$WORKING_DIR")"
temp_bgen="$temp_bgen".bgen

# Should I re-direct STDERR to /dev/null?
info_msg "converting gen->bgen..."
qctool -g "$temp_gen_data" -og "$temp_bgen" -s "$temp_gen_sample"

if [[ $FLAGS_keep_vcf -eq 0 ]] && [[ $run_filter -eq 0 ]]; then
    mv "$temp_filter_vcf" "$outdir"
fi

# Create a bgenix index on our output file
info_msg "indexing bgen..."
bgenix -g "$temp_bgen" -index

# Relocate everything back from scratch
mv "$temp_bgen" "$output_bgen"
mv "$temp_bgen".bgi "$output_bgen".bgi

# Output some stats
info_msg "initial VCF samples: $initial_samples"
info_msg "initial VCF variants: $nvars"
info_msg "gen file variants: $nvar_gen"
info_msg "*** END ***"
