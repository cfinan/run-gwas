#!/bin/bash
# A task for running a bgenie on a genomic chunk

# The missing data parameter used by bgenie
MISSING="-999"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Get the first variant ID present in the .bgi index
#
# Globals Required:
#   None
# Arguments:
#   $1: The path to a .bgi (bgen index) file
# Globals Set:
#   None
# Returns:
#   Nothing
# Outputs:
#   The ID if the search SNP (the first variant in BGI file)
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
get_search_snp() {
    local bgi_file="$1"
    search_snp=$(sqlite3 "$bgi_file" "select rsid from variant limit 1")
    echo "$search_snp"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Output a file of samples and their index values (sample position) in the BGEN
# file. The BGEN index value is at column 1 and the sample ID is at column 2.
# The output file is a tab delimited and has a header of `bgen_idx`,
# `sample_id` and is sorted on `sample_id`.
#
# Globals Required:
#   None
# Arguments:
#   $1: The BGEN file to get the sample IDs from
#   $2: The variant to use as a proxy to extract samples as a VCF file
#   $3: The outfile to write the sample IDs to
# Globals Set:
#   None
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
get_sample_list() {
    local bgen_file="$1"
    local search_snp="$2"
    local outfile="$3"

    # Here is what is going on here:
    # 1. Use the search SNP to output a single row of genotypes (with sample
    #    IDs) in VCF format.
    # 2. Then extract the sample line from the VCF header
    # 3. Substitute the tabs in the VCF sampl line for newlines
    # 4. Then remove the first 9 columns from the sample header (the
    #    non-sample columns)
    # 5. Output the sample IDs with their position in the sample header
    # 6. Sort the file on the sample ID
    bgenix -g "$bgen_file" -incl-rsids "$search_snp" -vcf 2>/dev/null |
        sed -n '/^#CHROM/p' |
        tr $'\t' '\n' |
        tail -n+10 |
        awk 'BEGIN {OFS="\t";print "bgen_idx","sample_id"} {$1=$1; print NR, $1}' |
        body sort -t$'\t' -k2,2 > "$outfile"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create a "working copy" of a file. The idea here is to make sure that the
# sample order of the working file is identical to the samples in the BGEN
# file. So samples present in the BGEN but missing from the source file are
# added to the working file with missing values. Samples present in the source
# file but not in the BGEN file are removed in the working file.
#
# Globals Required:
#   None
# Arguments:
#   $1: The sample IDs from the BGEN file with their order in the BGEN file
#   $2: The source file to compare to the BGEN samples and output to the
#       working file.
#   $3: The working file name that can be used for BGENIE
# Globals Set:
#   None
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
create_working_file() {
    local bgen_samples="$1"
    local source_file="$2"
    local working_file="$3"
    local add_dummy="$4"

    # Calculate the expected column number in the final file
    ncols=$(head -n1 "$source_file" | tr $'\t' '\n' | wc -l)
    ncols=$(( ncols + 1 ))

    temp_working="$(mktemp -p"$WORKING_DIR")"

    cat $source_file |
        body sort -t$'\t' -k1,1 |
        join --header -t$'\t' -a1 -12 -21 "$bgen_samples" - |
        awk -vexpcols="$ncols"\
            -vmiss="$MISSING" \
            'BEGIN{FS="\t"; OFS="\t"}{if ($3 == ""){for (i=3; i<=expcols; i++) {$i=miss}} print $0}' |
        body sort -t$'\t' -k2n,2n |
        cut -f1-2 --complement |
        tr $'\t' ' ' > "$temp_working"

	if [[ $add_dummy -eq 1 ]]; then
		# awk '{if (NR > 1) {print $0, NR % 2} else {print $0, "dummy_12345"}}' \
	    	# "$temp_working" > "$working_file"
		mv "$temp_working" "$working_file"
	    #peep -d' ' "$working_file"
	else
		mv "$temp_working" "$working_file"
	fi
	
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The main command that is run for each step of --step
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
run_command() {
    # Check that the programs are present
    # The traps will catch errors where they are not present
    # note these must be in your PATH and names properly
    info_msg "checking bgenix is in your PATH (error at this point means no)"
    bgenix -help >/dev/null 2>&1
    info_msg "checking bgenie is in your PATH (error at this point means no)"
    bgenie --version >/dev/null 2>&1
    info_msg "checking sqlite3 is in your PATH (error at this point means no)"
    sqlite3 -version >/dev/null 2>&1

    # Pull the columns from the current row
    region_idx="${PROCESS_LINE[1]}"
    chr_name="${PROCESS_LINE[2]}"
    start_pos="${PROCESS_LINE[3]}"
    end_pos="${PROCESS_LINE[4]}"
    exp_sites="${PROCESS_LINE[5]}"
    bgen_file="${PROCESS_LINE[6]}"
    results_file="${PROCESS_LINE[7]}"
    phenofile="${PROCESS_LINE[8]}"
    covfile="${PROCESS_LINE[9]}"

    # TODO: Check that the file based arguments are readable/writable
    # (including the bgi index)
    info_msg "region_idx: $region_idx"
    info_msg "chr_name: $chr_name"
    info_msg "start_pos: $start_pos"
    info_msg "end_pos: $end_pos"
    info_msg "exp_sites: $exp_sites"
    info_msg "bgen file: $bgen_file"
    info_msg "results file: $results_file"
    info_msg "phenofile: $phenofile"
    info_msg "covfile: $covfile"

    # Is the bgen file (and the .bgi index file readable)
    bgi_file="$bgen_file".bgi
    check_file_readable "$bgen_file" "BGEN file not readable"
    check_file_readable "$bgi_file" "BGEN INDEX file not readable"

    # Make sure the the phenotype and covariates files are readable
    check_file_readable "$phenofile" "phenotype file not readable"

    # The covariate file might not be present so only check if it is
    if [[ "$covfile" != '-' ]]; then
        check_file_readable "$covfile" "covariate file not readable"
    fi

    # Make sure that the results file would be writable
    check_file_writable "$results_file" "results file file not writable"

    # Now we want to get a list of all the samples in the current BGEN
    # file that we ware working on. We so this in an indirect way by
    # extracting data for a single variant (the first one) as a VCF
    # file and grabbing the sample IDs from that. It is a bit of a hack
    # but it does mean that we do not need qctool to be present
    search_snp="$(get_search_snp "$bgi_file")"
    info_msg "the search variant is: $search_snp"

    # This will produce a file with a bgen row position in column 1
    # (so this is the order in the bgen file) and the sample ID in
    # column 2. The file is sorted on sample ID but can be re-arranged
    # into "bgen order" by sorting on column 1 at any time
    sample_list="$(mktemp -p"$WORKING_DIR")"
    info_msg "writing bgen sample list to: $sample_list"
    get_sample_list "$bgen_file" "$search_snp" "$sample_list"

    nbgen_samples="$(wc -l $sample_list | cut -f1 -d" ")"
    info_msg "there are $nbgen_samples samples in bgen file"

    npheno_samples="$(wc -l $phenofile | cut -f1 -d" ")"
    ncov_samples="$(wc -l $covfile | cut -f1 -d" ")"
    info_msg "there are $npheno_samples samples in phenotype file"
    info_msg "there are $ncov_samples samples in covariate file"

    # Now create a working phenotype file
    working_pheno="$(mktemp -p"$WORKING_DIR")"
    create_working_file "$sample_list" "$phenofile" "$working_pheno" 1
    npheno_samples="$(wc -l $working_pheno | cut -f1 -d" ")"
    info_msg "there are $npheno_samples samples in the working phenotype file"
    temp_results="$(mktemp -p"$WORKING_DIR")"

    app_args=(
        --bgen "$bgen_file"
        --pheno "$working_pheno"
        --pvals
        --miss "$MISSING"
        --range "$chr_name" "$start_pos" "$end_pos"
        --thread 1
        --out "$temp_results"
    )

    # If there is a covariate file then process it
    if [[ "$covfile" != '-' ]]; then
        # Now create a working covarate file
        working_cov="$(mktemp -p"$WORKING_DIR")"
        create_working_file "$sample_list" "$covfile" "$working_cov" 0
        ncov_samples="$(wc -l $working_cov | cut -f1 -d" ")"
        info_msg "there are $ncov_samples samples in working covariate file"

        # Update the app arguments
        app_args+=(--covar "$working_cov")
    fi

    bgenie "${app_args[@]}"

    result_rows="$(zcat "$temp_results".gz | wc -l)"
    result_rows=$(( result_rows - 1 ))
    info_msg "there are $result_rows variants in the results file"

    if [[ $result_rows -ne $exp_sites ]]; then
        error_msg "unexpected results rows: $result_rows (actual) != $exp_sites (expected)"
        # error_exit "$LINENO"
    fi
	mv "$temp_results".gz "$results_file"
}

# This actually initialises the running of the job
. task_init.sh
