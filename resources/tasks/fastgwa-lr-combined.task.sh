#!/bin/bash
# A task for running a fastGWA-lr analysis over multiple bgens
# Assumptions:
# 1. All the sample files are the same
# 2. All the bgen files are indexed with bgenix
# 3. That the FID & IID sample IDs are the same

# The missing data parameter used by bgenie
MISSING="-9"


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Check all the files in the list of bgen files are present and correct with
# index files and sample files. This also ensures that the sample files have
# exactly the same sample IDs in them (not sure what fastGWA does if not).
#
# Globals Required:
#   None
# Arguments:
#   $1: The path to the bgen list file (one file per line)
# Globals Set:
#   $sample_file
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
check_bgen_list() {
    # Note sets a global sample_file variable
    local bgen_list="$1"
    local md5_value=""
    # Make sure we have bgen .bgi and sample files for all files listed
    # in the bgen list file and compare the sample files to make sure they
    # are all the same
    while read -r line; do
        check_file_readable "$line" \
                            "BGEN file not readable: $line"
        check_file_readable "$line".bgi \
                            "BGEN index file not readable: ${line}.bgi"
        sample_file=$(echo "$line"| sed 's/\.bgen/.sample/')
        check_file_readable "$sample_file" \
                            "sample file not readable: $sample_file"

        # If no md5sum has been set then initialise
        if [[ "$md5_value" == "" ]]; then
            md5_value=$(md5sum "$sample_file" | cut -f1 -d' ')
        fi

        # check the md5sum is the same
        local cur_md5=$(md5sum "$sample_file" | cut -f1 -d' ')
        if [[ "$cur_md5" != "$md5_value" ]]; then
            error_msg "md5 is different for sample file: $sample_file"
            error_exit $LINENO
        fi
    done < "$bgen_list"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# This builds a master sample file with the first column being the sample ID
# and the second being the order of samples in the bgen file (integer). The
# final file is sorted on sample ID not the bgen order. This is so it can be
# joined with other phenotype files and the order can then be reset down the
# line to the order they occur in the bgen file. The final output file is tab
# delimited.
#
# Globals Required:
#   None
# Arguments:
#   $1: The path to the input sample file (as in snptest sample file) with a
#       header line, data type line then the samples. The first two columns
#       should be FID, IID. Only the IID is carried through to the lookup file
#       as the assumption is that the FID and IID are the same.
#   $2: The output sample lookup file name.
# Globals Set:
#   None
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_sample_lookup_file() {
    local sample_input_file="$1"
    local temp_sample_file="$2"

    tail -n+3 "$sample_input_file" |
        awk 'BEGIN{OFS="\t";print "IID","bgen_order"}{$1=$1; print $2,NR}' |
        body sort -k1,1 > "$temp_sample_file"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create a "working copy" of a file. The idea here is to make sure that the
# sample order of the working file is identical to the samples in the BGEN
# file. So samples present in the BGEN but missing from the source file are
# added to the working file with missing values. Samples present in the source
# file but not in the BGEN file are removed in the working file. The working
# file might be a phenotype file or a covariates file.
#
# Globals Required:
#   $WORKING_DIR: A corking directory to place temp intermediate files.
#   $MISSING: A value to use for missing data in the working file (columns that
#             have no data in the source file that are present in the working
#             file).
# Arguments:
#   $1: The sample IDs from the BGEN file with their order in the BGEN file
#       The expectation is that the sample ID is in column 1 and the BGEN
#       order (integer counter column) is in column 2.
#   $2: The source file to compare to the left join to BGEN samples and
#       output to the working file. The expectation is that the sample ID
#       column is the first column and the file is tab delimited and the file
#       has a header.
#   $3: The output working file name that can be used for fastGWA.
# Globals Set:
#   None
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_working_file() {
    local bgen_samples="$1"
    local source_file="$2"
    local working_file="$3"

    # Calculate the expected column number in the final file
    ncols=$(head -n1 "$source_file" | tr $'\t' '\n' | wc -l)
    ncols=$(( ncols + 1 ))

    local temp_working="$(mktemp -p"$WORKING_DIR")"

    # 1. Sort the source file by the first column (sample IDs)
    cat $source_file |
        body sort -t$'\t' -k1,1 |
        join --header -t$'\t' -a1 -11 -21 "$bgen_samples" - |
        awk -vexpcols="$ncols"\
            -vmiss="$MISSING" \
            'BEGIN{FS="\t"; OFS="\t"}{if ($3 == ""){for (i=3; i<=expcols; i++) {$i=miss}} print $0}' |
        body sort -t$'\t' -k2n,2n |
        cut -f2 --complement > "$temp_working"
    mv "$temp_working" "$working_file"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create a "working copy" of the phenotype file that will be used to run
# fastGWA. This will extract the required phenotype value from a master
# phenotype file and create an output file with the same order as the samples
# in the BGEN file with only the required phenotype value within it.
#
# Globals Required:
#   $WORKING_DIR: A working directory to place temp intermediate files.
# Arguments:
#   $1: The sample IDs from the BGEN file with their order in the BGEN file
#       The expectation is that the sample ID is in column 1 and the BGEN
#       order (integer counter column) is in column 2. Should be tab delimited.
#   $2: The master phenotype file, with a sample ID column and the phenotype
#       values. Should be tab delimited.
#   $3: The name of the phenotype column to extract from the master phenotype
#       file.
#   $4: The name of the sample ID column in the master phenotype column.
#   $5: The output phenotype file, this will be used for input into
#       fastGWA. This will be tab delimited.
# Globals Set:
#   None
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_phenotype_file() {
    local sample_lookup="$1"
    local pheno_file="$2"
    local gwas_phenotype="$3"
    local sample_id_colname="$4"
    local out_pheno_file="$5"

    local temp_pheno=$(mktemp -p"$WORKING_DIR")

    # Extract the phenotype we are interested in
    rejig -i"$pheno_file" -- "$sample_id_colname" "$gwas_phenotype" |
        sort -k1,1 -t$'\t' > "$temp_pheno"
    build_working_file "$sample_lookup" "$temp_pheno" "$out_pheno_file"
    output_expected_samples "$working_pheno" "phenotype"
    add_fid "$working_pheno"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create a "working copy" of the covariate file that will be used to run
# fastGWA. This will create an output file with the same order as the samples
# in the BGEN file.
#
# Globals Required:
#   $WORKING_DIR: A working directory to place temp intermediate files.
# Arguments:
#   $1: The sample IDs from the BGEN file with their order in the BGEN file
#       The expectation is that the sample ID is in column 1 and the BGEN
#       order (integer counter column) is in column 2. Should be tab delimited.
#   $2: The master covariate file, with a sample ID column and the phenotype
#       values. Should be tab delimited.
#   $3: The output covariate file, this will be used for input into
#       fastGWA. This will be tab delimited.
#   $4: The covariate type. This is used for logging the numbers of samples to
#       STDOUT
# Globals Set:
#   None
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_covar_file() {
    local sample_lookup="$1"
    local covar_file="$2"
    local working_file="$3"
    local covar_type="$4"

    check_file_readable "$covar_file" \
                        "$covar_type covariate file not readable"
    create_working_file "$sample_lookup" \
                        "$covar_file" \
                        "$working_file"
    output_expected_samples "$working_file" "$covar covariates"
    add_fid "$working_file"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
update_keep_file() {
    local master_keep_file="$1"
    local test_file="$2"
    test_keep_file="$(create_keep_file "$test_file")"
    local temp_keep="$(mktemp -p"$WORKING_DIR")"
    join --header -11 -21 "$master_keep_file" "$test_keep_file" > "$temp_keep"
    rm "$master_keep_file"
    rm "$test_keep_file"
    mv "$temp_keep" "$master_keep_file"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
build_keep_file() {
    local source_file="$1"
    local temp_working="$(mktemp -p"$WORKING_DIR")"
    awk -vmiss="$MISSING" \
        'BEGIN{FS="\t";OFS="\t"}{has_missing=0;for (i=3; i<=NF; i++) {if ($i == miss) {has_missing=1}} if (has_missing==0) {print $2}}' "$source_file" |
        body sort > "$temp_working"
    echo "$temp_working"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
output_expected_samples() {
    local test_file="$1"
    local file_type="$2"
    total_samples="$(cat "$test_file" | wc -l)"
    nsamples=$(
        awk -vmiss="$MISSING" \
            'BEGIN{FS="\t";OFS="\t"}{has_missing=0;for (i=2; i<=NF; i++) {if ($i == miss) {has_missing=1}} if (has_missing==0) {print $1}}' "$test_file" | wc -l
            )
    info_msg "non-missing $file_type: $((nsamples - 1))/$((total_samples - 1))"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
add_fid() {
    local proc_file="$1"
    local temp_working="$(mktemp -p"$WORKING_DIR")"

    awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {print $1,$0} else {$1="IID";print "FID",$0}}' "$proc_file" > "$temp_working"
    mv "$temp_working" "$proc_file"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The main command that is run for each step of --step
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
run_command() {
    # Pull the columns from the current row
    # PROCESS_LINE[0] is extracted before this call
    # Should a GCTA module be loaded prior to processing
    module_load="${PROCESS_LINE[1]}"
    # The path to the bgen file
    bgen_list_file="${PROCESS_LINE[2]}"
    # The path to the source phenotype file, this will be processed into a
    # working phenotype file
    pheno_file="${PROCESS_LINE[3]}"
    # The path to a binary covariates file. If there are no covariates then
    # this should be set to - . If supplied then the expectation is that it
    # is tab delimited with a header and the sample ID is in the first column
    # and subsequent columns are phenotypes.
    binary_covars_file="${PROCESS_LINE[4]}"
    # The path to a quantitative covariates file. If there are no covariates
    # then this should be set to -
    quant_covars_file="${PROCESS_LINE[5]}"
    # The phenotype column being GWAS'd this should be a column name in
    # pheno_file.
    gwas_phenotype="${PROCESS_LINE[6]}"
    # The expected results file, this is checked by pyjasub to indicate if a
    # task should be run
    results_file="${PROCESS_LINE[7]}"
    # The number of autosomes
    autosome_number="${PROCESS_LINE[8]}"

    info_msg "module_load: $module_load"
    info_msg "bgen list file: $bgen_list_file"
    info_msg "phenotype file: $pheno_file"
    info_msg "binary covariates file: $binary_covars_file"
    info_msg "quantitative covariates file: $quant_covars_file"
    info_msg "gwas phenotypes: $gwas_phenotype"
    info_msg "results file: $results_file"
    info_msg "autosome number: $autosome_number"

    # To cope with clusters using the module system, such as UCL
    if [[ "$module_load" -eq 1 ]]; then
        unset_run_env
        . ~/.bashrc
        module load gcta
        set_run_env
    fi

    # Check that the programs are present
    # The traps will catch errors where they are not present
    # note these must be in your PATH and names properly
    info_msg "checking bgenix is in your PATH (error at this point means no)"
    bgenix -help >/dev/null 2>&1
    info_msg "checking sqlite3 is in your PATH (error at this point means no)"
    sqlite3 -version >/dev/null 2>&1
    info_msg "checking gcta64 is in your PATH (error at this point means no)"
    which gcta64 >/dev/null 2>&1

    check_file_readable "$bgen_list_file" "BGEN list file not readable"

    # Make sure the bgens in the list file are valid
    check_bgen_list "$bgen_list_file"

    # Now we know all the sample files are the same we can just use
    # one of them, I am not sure how fastGWA handles differing sample
    # files?
    info_msg "using sample file: $sample_file"

    # Make sure the the phenotype and covariates files are readable
    check_file_readable "$pheno_file" "phenotype file not readable"

    app_args=(
        --fastGWA-lr
        --threads 1
        --nofilter
        --autosome-num "$autosome_number"
        --mbgen "$bgen_list_file"
        --sample "$sample_file"
    )

    # Now process the phenotype file, this involves extracting the
    # phenotype and also subsetting the bgen samples
    sample_id_colname=$(head -n1 "$pheno_file" | cut -f1)

    # Make sure that the results file would be writable
    check_file_writable "$results_file" "results file file not writable"

    # Extract the available samples from the bgen file, log the row order then
    # order them based on the sample ID
    temp_bgen_samples=$(mktemp -p"$WORKING_DIR")
    build_sample_lookup_file "$sample_file" "$temp_bgen_samples"

    # output the number of samples in the bgen files
    nbgen_samples="$(wc -l $temp_bgen_samples | cut -f1 -d" ")"
    info_msg "there are $((nbgen_samples - 1)) samples in bgen file"

    working_pheno="$(mktemp -p"$WORKING_DIR")".txt
    build_phenotype_file "$temp_bgen_samples" \
                         "$pheno_file" \
                         "$gwas_phenotype" \
                         "$sample_id_colname" \
                         "$working_pheno"
    app_args+=(--pheno "$working_pheno")

    # Keep file is passed to fastGWA to only process the samples
    # with no missing data
    keep_file="$(build_keep_file "$working_pheno")"

    # The binary covariate file might not be present so only
    # check if it is
    if [[ "$binary_covars_file" != '-' ]]; then
        working_bin_covar="$(mktemp -p"$WORKING_DIR")"
        build_covar_file "$temp_bgen_samples" \
                         "$binary_covars_file" \
                         "$working_bin_covar" \
                         "binary"
        update_keep_file "$keep_file" "$working_bin_covar"
        app_args+=(--covar "$working_bin_covar")
    else
        working_bin_covar='-'
    fi

    # The quantitative covariate file might not be present so
    # only check if it is
    if [[ "$quant_covars_file" != '-' ]]; then
        working_quant_covar="$(mktemp -p"$WORKING_DIR")"
        build_covar_file "$temp_bgen_samples" \
                         "$quant_covars_file" \
                         "$working_quant_covar" \
                         "quantitative"
        update_keep_file "$keep_file" "$working_quant_covar"
        app_args+=(--qcovar "$working_quant_covar")
    else
        working_quant_covar='-'
    fi

    working_keep=$(mktemp -p"$WORKING_DIR")
    join --header -t$'\t' -11 -21 "$temp_bgen_samples" "$keep_file" |
        sort -t$'\t' -k2n,2 |
        cut -f1 > "$working_keep"
    add_fid "$working_keep"
    nsamples=$(tail -n+2 "$working_keep" | wc -l)
    info_msg "expected sample size: $nsamples"
    app_args+=(--keep "$working_keep")

    # Now create a working output file in temp
    outdir="$(dirname "$results_file")"
    info_msg "output dir: $outdir"

    outbase="$(basename "$results_file" | sed 's/\.fastGWA\.gz//')"
    exp_out_base="$(basename "$results_file" | sed 's/\.gz//')"
    temp_out="$WORKING_DIR"/"$outbase"
    info_msg "temp output file: $temp_out"
    app_args+=(--out "$temp_out")

    echo "${app_args[@]}"
    # cp -r "$WORKING_DIR" ~/Scratch/gcta_test_combined
    gcta64 ${app_args[@]}
    gzip "$WORKING_DIR"/"$exp_out_base"
    # cp -r "$WORKING_DIR" ~/Scratch/gcta_test_combined
    md5out="$temp_out".md5
    info_msg "md5 output: $md5out"
    echo -ne "$(basename "$results_file")\t" > "$md5out"
    zcat "$WORKING_DIR"/"$exp_out_base" | md5sum | cut -f1 -d' ' >> "$md5out"
    info_msg "moving md5 file: $md5out -> $outdir"
    mv "$md5out" "$outdir"
    info_msg "moving log file: ${temp_out}.log $outdir"
    mv "$temp_out".log "$outdir"
    info_msg "moving output file: ${temp_out}.fastGWA.gz $outdir"
    mv "$temp_out".fastGWA.gz "$outdir"
}

# Source in the bash helpers bio set of functions, that provide a function
# for getting the samples from a bgen file
. bh_bio.sh

# This actually initialises the running of the job
. task_init.sh
