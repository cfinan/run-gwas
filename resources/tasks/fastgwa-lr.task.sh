#!/bin/bash
# A task for running a bgenie on a genomic chunk

# The missing data parameter used by bgenie
MISSING="-9"

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# Create a "working copy" of a file. The idea here is to make sure that the
# sample order of the working file is identical to the samples in the BGEN
# file. So samples present in the BGEN but missing from the source file are
# added to the working file with missing values. Samples present in the source
# file but not in the BGEN file are removed in the working file.
#
# Globals Required:
#   None
# Arguments:
#   $1: The sample IDs from the BGEN file with their order in the BGEN file
#       The expectation is that the sample ID is in column 1 and the BGEN
#       order (integer counter column) is in column 2.
#   $2: The source file to compare to the left join to BGEN samples and
#       output to the working file.
#   $3: The working file name that can be used for fastGWA.
# Globals Set:
#   None
# Returns:
#   None
# Outputs:
#   Nothing
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
create_working_file() {
    local bgen_samples="$1"
    local source_file="$2"
    local working_file="$3"
    # local add_dummy="$4"

    # Calculate the expected column number in the final file
    ncols=$(head -n1 "$source_file" | tr $'\t' '\n' | wc -l)
    ncols=$(( ncols + 1 ))

    local temp_working="$(mktemp -p"$WORKING_DIR")"

    cat $source_file |
        body sort -t$'\t' -k1,1 |
        join --header -t$'\t' -a1 -11 -21 "$bgen_samples" - |
        awk -vexpcols="$ncols"\
            -vmiss="$MISSING" \
            'BEGIN{FS="\t"; OFS="\t"}{if ($3 == ""){for (i=3; i<=expcols; i++) {$i=miss}} print $0}' |
        body sort -t$'\t' -k2n,2n |
        cut -f2 --complement > "$temp_working"
        # tr $'\t' ' ' > "$temp_working"
    mv "$temp_working" "$working_file"
	# if [[ $add_dummy -eq 1 ]]; then
	# 	# awk '{if (NR > 1) {print $0, NR % 2} else {print $0, "dummy_12345"}}' \
	#     	# "$temp_working" > "$working_file"
	# 	mv "$temp_working" "$working_file"
	#     #peep -d' ' "$working_file"
	# else
	# 	mv "$temp_working" "$working_file"
	# fi
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
create_keep_file() {
    local source_file="$1"
    local temp_working="$(mktemp -p"$WORKING_DIR")"
    awk -vmiss="$MISSING" \
        'BEGIN{FS="\t";OFS="\t"}{has_missing=0;for (i=3; i<=NF; i++) {if ($i == miss) {has_missing=1}} if (has_missing==0) {print $1"-"$2}}' "$source_file" |
        body sort > "$temp_working"
    echo "$temp_working"
}

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
output_expected_samples() {
    local test_file="$1"
    local file_type="$2"
    total_samples="$(cat "$test_file" | wc -l)"
    # awk -vmiss="$MISSING" \
    #     'BEGIN{FS="\t";OFS="\t"}{has_missing=0;for (i=2; i<=NF; i++) {if ($i == miss) {print $i; has_miss=1}} if (has_missing==1) {print $0}}' "$test_file"
    nsamples=$(
        awk -vmiss="$MISSING" \
            'BEGIN{FS="\t";OFS="\t"}{has_missing=0;for (i=2; i<=NF; i++) {if ($i == miss) {has_missing=1}} if (has_missing==0) {print $1}}' "$test_file" | wc -l
            )
    info_msg "non-missing $file_type: ${nsamples}/${total_samples}"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
add_fid() {
    local proc_file="$1"
    local temp_working="$(mktemp -p"$WORKING_DIR")"

    awk 'BEGIN{FS="\t";OFS="\t"}{if (NR>1) {print $1,$0} else {$1="IID";print "FID",$0}}' "$proc_file" > "$temp_working"
    mv "$temp_working" "$proc_file"
}


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The main command that is run for each step of --step
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
run_command() {
    # Pull the columns from the current row
    # PROCESS_LINE[0] is extracted before this call
    # Should a GCTA module be loaded prior to processing
    module_load="${PROCESS_LINE[1]}"
    # The path to the bgen file
    bgen_file="${PROCESS_LINE[2]}"
    # The path to the source phenotype file, this will be processed into a
    # working phenotype file
    pheno_file="${PROCESS_LINE[3]}"
    # The path to a binary covariates file. If there are no covariates then
    # this should be set to - . If supplied then the expectation is that it
    # is tab delimited with a header and the sample ID is in the first column
    # and subsequent columns are phenotypes.
    binary_covars_file="${PROCESS_LINE[4]}"
    # The path to a quantitative covariates file. If there are no covariates
    # then this should be set to -
    quant_covars_file="${PROCESS_LINE[5]}"
    # The phenotype column being GWAS'd this should be a column name in
    # pheno_file.
    gwas_phenotype="${PROCESS_LINE[6]}"
    # The expected results file, this is checked by pyjasub to indicate if a
    # task should be run
    results_file="${PROCESS_LINE[7]}"
    # Should a subset of the bgen file be extracted and run? If so the
    # chr_name, start_pos and end_pos all should be defined. These will be
    # used with bgenix to extract a temp bgen for GWASing
    chr_name="${PROCESS_LINE[8]}"
    start_pos="${PROCESS_LINE[9]}"
    end_pos="${PROCESS_LINE[10]}"
    # The number of expected sites in the extracted chunk, if set to 0 then
    # this is not checked
    expected_sites="${PROCESS_LINE[11]}"

    # TODO: Check that the file based arguments are readable/writable
    # (including the bgi index)
    info_msg "module_load: $module_load"
    info_msg "bgen file: $bgen_file"
    info_msg "phenotype file: $pheno_file"
    info_msg "binary covariates file: $binary_covars_file"
    info_msg "quantitative covariates file: $quant_covars_file"
    info_msg "gwas phenotypes: $gwas_phenotype"
    info_msg "results file: $results_file"
    info_msg "chromosome name: $chr_name"
    info_msg "start position: $start_pos"
    info_msg "end position: $end_pos"
    info_msg "expected sites: $expected_sites"

    # To cope with clusters using the module system, such as UCL
    # if [[ "$module_load" -eq 1 ]]; then
    #     unset_run_env
    #     . ~/.bashrc
    #     module load gcta
    #     set_run_env
    # fi

    # Check that the programs are present
    # The traps will catch errors where they are not present
    # note these must be in your PATH and names properly
    info_msg "checking bgenix is in your PATH (error at this point means no)"
    bgenix -help >/dev/null 2>&1
    info_msg "checking sqlite3 is in your PATH (error at this point means no)"
    sqlite3 -version >/dev/null 2>&1
    info_msg "checking gcta64 is in your PATH (error at this point means no)"
    which gcta64 >/dev/null 2>&1

    # Is the bgen file (and the .bgi index file readable)
    bgi_file="$bgen_file".bgi
    check_file_readable "$bgen_file" "BGEN file not readable"
    check_file_readable "$bgi_file" "BGEN INDEX file not readable"

    # Make sure the the phenotype and covariates files are readable
    check_file_readable "$pheno_file" "phenotype file not readable"

    app_args=(
        --fastGWA-lr
        --threads 1
        --nofilter
    )

    # Now process the phenotype file, this involves extracting the
    # phenotype and also subsetting the bgen samples
    sample_id_colname=$(head -n1 "$pheno_file" | cut -f1)

    # Make sure that the results file would be writable
    check_file_writable "$results_file" "results file file not writable"

    # Extract the available samples from the bgen file, log the row order then
    # order them based on the sample ID
    temp_bgen_samples=$(mktemp -p"$WORKING_DIR")

    unset_run_env
    get_bgen_samples "$bgen_file" |
        tr ' ' '\n' |
        awk 'BEGIN{OFS="\t";print "IID","sample_row_n"}{$1=$1; print $1,NR}' |
        body sort -t$'\t' -k1,1 > "$temp_bgen_samples"
    set_run_env

    nbgen_samples="$(wc -l $temp_bgen_samples | cut -f1 -d" ")"
    info_msg "there are $((nbgen_samples - 1)) samples in bgen file"
    # peep "$temp_bgen_samples"
    temp_pheno=$(mktemp -p"$WORKING_DIR")

    # Extract the phenotype we are interested in
    # TODO: edit for different missing values
    # TODO: remove the sort in development
    rejig -i"$pheno_file" -- "$sample_id_colname" "$gwas_phenotype" |
        awk 'BEGIN{FS="\t";OFS="\t"}{if ($2==-999) {$2=""} print $0}' |
        sort -k1,1 -t$'\t' -u > "$temp_pheno"
    working_pheno="$(mktemp -p"$WORKING_DIR")".txt
    create_working_file "$temp_bgen_samples" "$temp_pheno" "$working_pheno"
    output_expected_samples "$working_pheno" "phenotype"
    add_fid "$working_pheno"
    app_args+=(--pheno "$working_pheno")
    keep_file="$(create_keep_file "$working_pheno")"

    # The binary covariate file might not be present so only
    # check if it is
    if [[ "$binary_covars_file" != '-' ]]; then
        check_file_readable "$binary_covars_file" \
                            "binary covariate file not readable"
        working_bin_covar="$(mktemp -p"$WORKING_DIR")"
        create_working_file "$temp_bgen_samples" \
                            "$binary_covars_file" \
                            "$working_bin_covar"
        output_expected_samples "$working_bin_covar" "binary covariates"
        add_fid "$working_bin_covar"
        app_args+=(--covar "$working_bin_covar")
        # bin_keep_file="$(create_keep_file "$working_bin_covar")"
        # temp_keep="$(mktemp -p"$WORKING_DIR")"
        # join --header -11 -21 "$keep_file" "$bin_keep_file" > "$temp_keep"
        # rm "$keep_file"
        # rm "$bin_keep_file"
        # mv "$temp_keep" "$keep_file"
    else
        working_bin_covar='-'
    fi

    # The quantitative covariate file might not be present so
    # only check if it is
    if [[ "$quant_covars_file" != '-' ]]; then
        check_file_readable "$quant_covars_file" \
                            "quantitative covariate file not readable"
        working_quant_covar="$(mktemp -p"$WORKING_DIR")"
        create_working_file "$temp_bgen_samples" \
                            "$quant_covars_file" \
                            "$working_quant_covar"
        output_expected_samples "$working_quant_covar" "quantitative covariates"
        add_fid "$working_quant_covar"
        app_args+=(--qcovar "$working_quant_covar")
        # quant_keep_file="$(create_keep_file "$working_quant_covar")"
        # temp_keep="$(mktemp -p"$WORKING_DIR")"
        # join --header -11 -21 "$keep_file" "$quant_keep_file" > "$temp_keep"
        # rm "$keep_file"
        # rm "$quant_keep_file"
        # mv "$temp_keep" "$keep_file"
    else
        working_quant_covar='-'
    fi

    working_bgen="$bgen_file"
    if [[ "$chr_name" != '-' ]] && [[ "$start_pos" != '-' ]] && [[ "$end_pos" != '-' ]]; then
        subset_range="${chr_name}:${start_pos}-${end_pos}"
        info_msg "subsetting bgen: $subset_range"
        working_bgen=$(mktemp -p"$WORKING_DIR").bgen
        bgenix -g"$bgen_file" -incl-range "$subset_range" > "$working_bgen"
        bgenix -g"$working_bgen" -index
    fi
    app_args+=(--bgen "$working_bgen")

    info_msg "working bgen file: $working_bgen"
    # Now create a working output file in temp
    outbase="$(basename "$results_file")"
    temp_out="$WORKING_DIR"/"$outbase"
    info_msg "temp output file: $temp_out"
    app_args+=(--out "$temp_out")

    # TODO: sample IDs must not have dashes
    # cat "$keep_file" | tr '-' $'\t' | join -t$'\t'
    working_keep=$(mktemp -p"$WORKING_DIR")
    add_fid "$temp_bgen_samples"
    awk 'BEGIN{FS="\t"; OFS="\t"}{print $1"-"$2,$3}' "$temp_bgen_samples" |
        join -t$'\t' --header  -11 -21 - "$keep_file" |
        body sort -t$'\t' -k2n,2 |
        cut -f1 |
        tr '-' $'\t' > "$working_keep"
    rm "$keep_file"
    # peep "$working_keep"
    nsamples=$(tail -n+2 "$working_keep" | wc -l)
    info_msg "expected sample size: $nsamples"
    app_args+=(--keep "$working_keep")
    cp -r "$WORKING_DIR" ~/Scratch/gcta_test
    # peep "$working_pheno"
    gcta64 ${app_args[@]}
}

# Source in the bash helpers bio set of functions, that provide a function
# for getting the samples from a bgen file
. bh_bio.sh
# This actually initialises the running of the job
. task_init.sh
