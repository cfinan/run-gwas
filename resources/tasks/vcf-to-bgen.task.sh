#!/bin/bash
# A task for running a conversion from vcf to bgen

# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
# The main command that is run for each step of --step
# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
run_command() {
    # Check that the programs are present
    # The traps will catch errors where they are not present
    # note these must be in your PATH and names properly
    info_msg "checking bcftools is in your PATH (error at this point means no)"
    bcftools --help >/dev/null 2>&1
    info_msg "checking qctool is in your PATH (error at this point means no)"
    qctool --version >/dev/null 2>&1
    info_msg "checking bgenix is in your PATH (error at this point means no)"
    bgenix --version >/dev/null 2>&1

    # Pull the columns from the current row
    # The path to the input vcf file
    vcf_input="${PROCESS_LINE[1]}"
    # The path to the output BGEN file
    bgen_output="${PROCESS_LINE[2]}"
    # Should be 0 if not or 1 if yes
    keep_vcf="${PROCESS_LINE[3]}"
    # Should be 0 if you do not want to set universal IDs or 1 if yes
    uni_id="${PROCESS_LINE[4]}"
    # The info value to subset on 0 means no subset > 0 means subset
    info_to_subset="${PROCESS_LINE[5]}"
    # The info field to use if info subsetting, the column needs to be there
    # but only relevant if info_to_subset > 0. R2 is a good default value
    info_field="${PROCESS_LINE[6]}"
    # Path to samples to subset or - for no sample subset
    sample_file="${PROCESS_LINE[7]}"
    # GP or GT
    geno_tag="${PROCESS_LINE[8]}"

    # We will run in verbose mode
    vcf_to_bgen_args=(-v -T"$WORKING_DIR")

    check_file_readable "$vcf_input" "VCF input file not readable"
    check_file_writable "$bgen_output" "BGEN output file not writable"

    unset_run_env
    if [[ "$keep_vcf" -eq 1 ]]; then
        vcf_to_bgen_args+=(--keep-vcf)
    fi

    if [[ "$uni_id" -eq 1 ]]; then
        vcf_to_bgen_args+=(--uni-id)
    fi

    if [[ "$info_to_subset" != "0" ]]; then
        vcf_to_bgen_args+=(--info "$info_to_subset")

        if [[ "$info_field" != "" ]] && [[ "$info_field" != "-" ]]; then
            vcf_to_bgen_args+=(--info-field "$info_field")
        fi
    fi

    # The covariate file might not be present so only check if it is
    if [[ "$sample_file" != '-' ]]; then
        check_file_readable "$sample_file" "sample subset file not readable: $sample_file"
        vcf_to_bgen_args+=(--samples "$sample_file")
    fi

    # The covariate file might not be present so only check if it is
    if [[ "$geno_tag" != '' ]]; then
        vcf_to_bgen_args+=(--geno-tag "$geno_tag")
    fi

    # Add the input/output args
    vcf_to_bgen_args+=("$vcf_input" "$bgen_output")

    # Regions can be pipe delimited
    regions=($(echo "${PROCESS_LINE[9]}" | sed 's/\s*|\s*/ /g'))

    # Add any regions we might have
    if [[ ${#regions[@]} -gt 0 ]]; then
        for i in "${regions[@]}"; do
            vcf_to_bgen_args+=("$i")
        done
    fi
    set_run_env

    # Now run the subset and conversion
    vcf-to-bgen.sh "${vcf_to_bgen_args[@]}"
}

# This actually initialises the running of the job
. task_init.sh
