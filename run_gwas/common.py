"""Functions common to many modules in the package
"""
# from gwas_norm import constants as con
# from gwas_norm.utils import genome_assemblies
# from collections import namedtuple
# from hashlib import md5
from contextlib import contextmanager
# import csv
# import gzip
# import re
import os
import shutil
import tempfile
import binascii
# import warnings
import sys


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def is_gzip(file_name):
    """
    Determine if a file is gzipped by looking at the first few bytes in the
    file

    Parameters
    ----------
    file_name : :obj:`str`
        A route to the file that needs checking

    Returns
    -------
    is_gzipped : bool
        `True` if it is gzipped `False` if not
    """
    with open(file_name, 'rb') as test:
        # We read the first two bytes and we will look at their values
        # to see if they are the gzip characters
        testchr = test.read(2)
        is_gzipped = False

        # Test for the gzipped characters
        if binascii.b2a_hex(testchr).decode() == "1f8b":
            # If it is gziped, then close it and reopen as a gzipped file
            is_gzipped = True

    return is_gzipped


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_tmp_file(**kwargs):
    """
    Initialise a temp file to work with. This differs from `tempfile.mkstemp`
    as the temp file is closed and only the file name is returned.

    Parameters
    ----------
    **kwargs
        Any arguments usually passed to `tempfile.mkstemp`
    """
    tmp_file_obj, tmp_file_name = tempfile.mkstemp(**kwargs)
    os.close(tmp_file_obj)
    return tmp_file_name


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
@contextmanager
def stdopen(filename, mode='rt', method=open, use_tmp=False, tmp_dir=None,
            **kwargs):
    """
    Provide either an opened file or STDIN/STDOUT if filename is not a file.

    Parameters
    ----------
    filename : str or :obj:`sys.stdin` or NoneType
        The filename to open. If `sys.stdin`, '-', '' or `NoneType` then
        `sys.stdin` is yielded otherwise the file is opened with `method`
    mode : str
        Should be the usual ``w/wt/wb/r/rt/rb` '' is interpreted as read
    method : :obj:`func`
        The open method to use (uses the standard `open` as a default)
    **kwargs
        Any other kwargs passed to method

    Yields
    ------
    fobj : :obj:`File` or `sys.stdin` or `sys.stdout`
        A place to read or write depending on mode
    """
    if mode == '' or 'r' in mode or mode is None:
        # Reading
        if filename not in [sys.stdin, '-', '', None]:
            fobj = method(filename, mode, **kwargs)
            yield fobj
            fobj.close()
        else:
            if 'b' in mode:
                yield sys.stdin.buffer
            else:
                yield sys.stdin
    elif 'w' in mode:
        # Writing
        if filename not in [sys.stdout, '-', '', None]:
            if use_tmp is True:
                tmpout = get_tmp_file(dir=tmp_dir)

                try:
                    fobj = method(tmpout, mode, **kwargs)
                    yield fobj
                except Exception:
                    fobj.close()
                    os.unlink(tmpout)
                    raise
                fobj.close()
                shutil.move(tmpout, filename)
            else:
                fobj = method(filename, mode, **kwargs)
                yield fobj
                fobj.close()
        else:
            if 'b' in mode:
                yield sys.stdout.buffer
            else:
                yield sys.stdout
    else:
        raise ValueError("unknown mode: {0}".format(mode))
