"""Parse through the positional information of a file and produce an output of
region boundaries that contain ~a target number of sites. The number of sites
might not be exactly the same as the target if there are multiple sites with
the same coordinate at the point where the target number is reached. In these
cases all sites at those coordinates are included in the chunk so the start/end
coordinate of all regions is unique.
"""
from run_gwas import (
    __version__,
    __name__ as pkg_name,
    common
)
from pyaddons import log
from tqdm import tqdm
import sqlite3
import argparse
import sys
import os
import re
import gzip
import tempfile
import shutil
# import pprint as pp


_PROG_NAME = "make-genomic-chunks"
"""The name of the script (`str`)
"""
_DESC = __doc__
"""The program description (`str`)
"""


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def main():
    """The main entry point for the script
    """
    # Initialise and parse the command line arguments
    parser = _init_cmd_args()
    args = _parse_cmd_args(parser)

    logger = log.init_logger(_PROG_NAME, verbose=args.verbose)
    log.log_prog_name(logger, pkg_name, __version__)
    log.log_args(logger, args)

    real_infiles = None
    try:
        working_dir = tempfile.mkdtemp(dir=args.tmp_dir)

        # if we are processing BGEN files then we will check for the presence
        # of .bgi indexes and convert those into temp text files that will be
        # used in the regular way
        if args.bgen is True:
            logger.info("converting .bgi to text (this may take a while)")
            real_infiles = args.infiles
            args.ref_allele = b'REF'
            args.infiles = convert_bgi_to_text(args.infiles, working_dir)

        create_chunk_file(
            args.infiles, args.outfile, target=args.target,
            delimiter=args.delimiter, comment_char=args.comment_char,
            tmp_dir=args.tmp_dir, chr_name=args.chr_name,
            start_pos=args.start_pos, end_pos=args.end_pos,
            ref_allele=args.ref_allele, verbose=args.verbose,
            out_dir=args.out_dir, out_ext=args.out_ext,
            pheno_file=args.pheno_file, cov_file=args.cov_file,
            proxy_infiles=real_infiles
        )
        log.log_end(logger)
    except (OSError, FileNotFoundError):
        raise
    except (BrokenPipeError, IOError, KeyboardInterrupt):
        # Python flushes standard streams on exit; redirect remaining
        # output to devnull to avoid another BrokenPipeError at shutdown
        devnull = os.open(os.devnull, os.O_WRONLY)

        try:
            os.dup2(devnull, sys.stdout.fileno())
        except Exception:
            pass
        log.log_interrupt(logger)
    finally:
        shutil.rmtree(working_dir)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _init_cmd_args():
    """Initialise the command line arguments and return the parser.

    Returns
    -------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.
    """
    parser = argparse.ArgumentParser(
        description="Output genomic intervals that contain ~<target> sites"
    )
    parser.add_argument(
        'target',
        type=int,
        help="The number of sites per interval"
    )
    parser.add_argument(
        '-i', '--infiles',
        nargs="*",
        type=str,
        help="An input file, if not provided then STDIN is used. Must be"
        " sorted on chr_name, start_pos, end_pos, can be gzip compressed"
    )
    parser.add_argument(
        '-o', '--outfile',
        type=str,
        help="An output file, if not provided then STDOUT is used"
    )
    parser.add_argument(
        '--out-dir',
        type=str,
        help="The output directory prefixed onto the outfile (if input is "
        "not STDIN)"
    )
    parser.add_argument(
        '--out-ext',
        type=str,
        help="The output file extension added onto the outfile (if input is "
        "not STDIN)"
    )
    parser.add_argument(
        '--pheno-file',
        type=str,
        default='-',
        help="The covariate file that will be added to the output if "
        "input is not STDIN"
    )
    parser.add_argument(
        '--cov-file',
        type=str,
        default='-',
        help="The covariate file that will be added to the output if "
        "input is not STDIN"
    )
    parser.add_argument(
        '-T', '--tmp-dir',
        type=str,
        help="A temp directory"
    )
    parser.add_argument(
        '-d', '--delimiter',
        default="\t",
        type=str,
        help=r"An input file delimiter (default='\t')"
    )
    parser.add_argument(
        '-c', '--comment-char',
        type=str,
        default='##',
        help="The comment character, lines starting with this are ignored"
        " (but still output) (default: ##)"
    )
    parser.add_argument(
        '-C', '--chr-name',
        type=str,
        default='#CHROM',
        help="The name of the chromosome column (default: #CHROM)"
    )
    parser.add_argument(
        '-S', '--start-pos',
        type=str,
        default='POS',
        help="The name of the start position column (default: POS)"
    )
    parser.add_argument(
        '-E', '--end-pos',
        type=str,
        default=None,
        help="The name of the end position column, if not there use "
        "--start-pos as --end-pos (default: POS)"
    )
    parser.add_argument(
        '-R', '--ref-allele',
        type=str,
        default=None,
        help="The name of the reference allele column (if present), if not"
        " if this is defined then the end position is calculated from the "
        "start position + length(ref) - 1"
    )
    parser.add_argument(
        '-v', '--verbose',
        action="store_true",
        help="Give more output (to <STDERR>)"
    )
    parser.add_argument(
        '--bgen',
        action="store_true",
        help="The input files are bgen format"
    )

    return parser


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _parse_cmd_args(parser):
    """Return the parsed arguments.

    Parameters
    ----------
    args : `argparse.ArgumentParser`
        The argparse parser object with arguments added.

    Returns
    -------
    args : `argparse.Namespace`
        The argparse namespace object.
    """
    args = parser.parse_args()
    if args.target <= 0:
        raise ValueError("target should be a positive integer")

    args.chr_name = args.chr_name.encode()
    args.start_pos = args.start_pos.encode()

    if args.end_pos is not None:
        args.end_pos = args.end_pos.encode()
    if args.ref_allele is not None:
        args.ref_allele = args.ref_allele.encode()

    args.delimiter = args.delimiter.encode()
    args.comment_char = args.comment_char.encode()

    if args.out_dir is not None:
        if args.out_dir != "INFILE" and os.path.isdir(args.out_dir) is False:
            raise NotADirectoryError("--out-dir does not exist")
        args.out_dir = args.out_dir.encode()

    args.cov_file = args.cov_file.encode()
    args.pheno_file = args.pheno_file.encode()

    return args


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def convert_bgi_to_text(infiles, outdir):
    """Convert a bgenix index to a test file so it can be processed like a VCF
    file.

    Parameters
    ----------
    infiles : `list` of `str`
        One or more ``.bgen`` files, these must have associated ``.bgi`` index
        files.
    outdir : `str`
        The output directory path, to put the converted index files.

    Returns
    -------
    outfiles : `list` of `str`
        The converted output text files.
    """
    index_files = []
    for i in infiles:
        # TODO: Check the actual bgen file exists??
        bgi_file = '{0}.bgi'.format(i)
        # bgi_file = i
        open(bgi_file).close()
        index_files.append(bgi_file)

    sql = """
    select chromosome, position, allele1
    from Variant
    order by chromosome, position
    """
    outfiles = []
    for i in index_files:
        outfile = "{0}.posfile".format(
            os.path.join(outdir, os.path.basename(i))
        )
        conn = sqlite3.connect(i)
        cur = conn.cursor()
        with gzip.open(outfile, 'wt') as outpos:
            outpos.write('#CHROM\tPOS\tREF\n')
            for row in cur.execute(sql):
                outpos.write('{0}\t{1}\t{2}\n'.format(row[0], row[1], row[2]))

        outfiles.append(outfile)
    return outfiles


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def create_chunk_file(infiles=None, outfile=None, target=10000,
                      delimiter=b"\t", comment_char=b"##", tmp_dir=None,
                      chr_name=b"#CHROM", start_pos=b"POS", end_pos=None,
                      ref_allele=None, verbose=False, proxy_infiles=None,
                      out_dir=None, out_ext=None, pheno_file=b'-',
                      cov_file=b'-'):
    """Create a chunk file from one or more input files

    This is the main entry point and can be used for an API call to create a
    chunk output file (or STDOUT).

    Parameters
    ----------
    infiles : `list` of `str`, optional, default: `NoneType`
        One or more output files, if `NoneType` or an empty list then it is
        assumed that the input is coming from STDIN.
    outfile : `str` or `NoneType`, optional, default: `NoneType`
        The output file, if `NoneType` it is assumed that the output is to
        STDOUT.
    target : `int`, optional, default: `10000`
        The target number of variants in each chunk. Some chunks will end up
        with more if the coordinate boundaries at ``target`` would result in
        the same coordinates being on two separate chunks.
    delimiter : `str`, optional, default: `\t`
        The delimiter of the input
    comment_char : `str`, optional, default: `##`
        Any comment characters that maybe present before the header is read in
    chr_name : `str`, optional, default: `#CHROM`
        The name of the chromosome column in the input files
    start_pos : `str`, optional, default: `POS`
        The name of the start position column in the input files.
    end_pos : `bytes`, optional, default: `NoneType`
        The end position column.
    ref_allele : `bytes`, optional, default: `NoneType`
        The ref allele column.
    verbose : `bool`, optional, default: `False`
        Should progress be reported.
    proxy_infiles : `bytes`, optional, default: `NoneType`
        TBC...
    out_dir : `str`, optional, default: `NoneType`
        An output directory name.
    out_ext : `str`, optional, default: `NoneType`
        An output file extension.
    pheno_file : `bytes`, optional, default: `-`,
        TBC...
    cov_file : `bytes`, optional, default: `-`
        TBC...
    """
    proxy_infiles = proxy_infiles or infiles

    # if the input is from STDIN we will not create an output file name
    # however, if it is then we will create an infile/outfile column so
    # the output can be used as a job array file
    out_header = [
        'rowidx', 'region_idx', 'chr_name', 'start_pos', 'end_pos', 'nsites'
    ]

    # Make sure the input files is represented in a standard way if we are
    # reading from STDIN
    if infiles is None or len(infiles) == 0:
        infiles = [None]
    else:
        out_header.extend(
            ['infile', 'outfile', 'pheno_file', 'covariate_file']
        )

    # A counter for the number of sites passed through
    idx = '0'

    # A counter for the number of regions output
    row_idx = '0'

    with common.stdopen(outfile, mode='wb', use_tmp=True,
                        tmp_dir=tmp_dir) as outchunks:
        # Write the output header
        outchunks.write("\t".join(out_header).encode() + b'\n')

        for idx, infile in enumerate(infiles):
            tqdm_kwargs = dict(
                disable=not verbose,
                unit=" variants",
                desc="processing file {0}/{1}...".format(
                    idx+1, len(infiles)
                )
            )

            for chunk in tqdm(
                    get_chunks(
                        infile=infile,
                        target=target,
                        delimiter=delimiter,
                        comment_char=comment_char,
                        tmp_dir=tmp_dir,
                        chr_name=chr_name,
                        start_pos=start_pos,
                        end_pos=end_pos,
                        ref_allele=ref_allele,
                        idx=int(idx) + 1,
                        row_idx=int(row_idx) + 1,
                        proxy_infile=proxy_infiles[idx],
                        out_dir=out_dir,
                        out_ext=out_ext,
                        pheno_file=pheno_file,
                        cov_file=cov_file
                    ), **tqdm_kwargs
            ):
                row_idx = chunk[0]
                idx = chunk[1]
                outchunks.write(b"\t".join(chunk) + b'\n')

                # For STDOUT mainly
                outchunks.flush()


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_chunks(infile=None, target=10000, delimiter=b"\t", comment_char=b"##",
               tmp_dir=None, chr_name=b"#CHROM", start_pos=b"POS",
               end_pos=None, ref_allele=None, idx=1, row_idx=1,
               out_dir=None, out_ext=None, pheno_file=b'-', cov_file=b'-',
               proxy_infile=None):
    """Yield chunk rows.
    """
    proxy_infile = proxy_infile or infile
    method = open
    if infile is not None and common.is_gzip(infile) is True:
        method = gzip.open

    file_name_func = _dummy
    root_file_name = ''
    file_ext = ''
    if infile is not None:
        file_name_func = _out_file_name
        root_file_name, file_ext = _split_ext(proxy_infile)

    # stdopen uses stdin if infile is not defined or -
    with common.stdopen(infile, mode='rb', method=method) as inpath:
        # Skip any of the leading comment rows (such as ## in VCFs)
        for row in inpath:
            if row.startswith(comment_char):
                pass
            else:
                # Not a comment row so we must be at the header
                header = row.strip().split(delimiter)
                break

        # ID the columns that we are interested in in the header
        chr_name_idx, start_pos_idx, end_pos_idx, end_pos_func = \
            find_in_header(
                header, chr_name, start_pos, end_pos,
                ref_allele
            )

        # Initialise the chr,start and end of the region. The end
        # will change as we progress through the file, the chr_name
        # and start will stay the same until a new region is created
        row = next(inpath).strip().split(delimiter)
        chr_name, start_pos, end_pos = end_pos_func(
            row[chr_name_idx], row[start_pos_idx], row[end_pos_idx]
        )

        for line in inpath:
            row = line.strip().split(delimiter)

            # The chr,start,end for the current line
            cur_chr, cur_start, cur_end = end_pos_func(
                row[chr_name_idx], row[start_pos_idx], row[end_pos_idx]
            )

            # if we have reached the target and are not in the middle
            # of an overlapping sequence run. or we have changed the
            # chromosome. Then we will output the region
            # if (idx >= target and cur_start != end_pos)
            if (idx >= target and cur_start > end_pos) or \
               cur_chr != chr_name:

                outrow = [
                    str(row_idx).encode(), str(row_idx).encode(),
                    chr_name, str(start_pos).encode(),
                    str(end_pos).encode(), str(idx).encode()
                ]
                outrow.extend(
                    file_name_func(
                        root_file_name, file_ext, row_idx, chr_name,
                        start_pos, end_pos, out_dir, out_ext, pheno_file,
                        cov_file
                    )
                )

                yield outrow

                # Reset for the next region
                chr_name, start_pos, end_pos = \
                    cur_chr, cur_start, cur_end
                idx = 0
                row_idx += 1

            # Update the index and the end position for the region
            idx += 1
            end_pos = max(end_pos, cur_end)

        # A final write so we have everything
        outrow = [
            str(row_idx).encode(), str(row_idx).encode(), chr_name,
            str(start_pos).encode(), str(end_pos).encode(),
            str(idx).encode()
        ]
        outrow.extend(
            file_name_func(
                root_file_name, file_ext, row_idx, chr_name,
                start_pos, end_pos, out_dir, out_ext, pheno_file,
                cov_file
            )
        )
        yield outrow


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def find_in_header(header, chr_name, start_pos, end_pos, ref_allele):
    """ Find the location of the chromosome name, start position and end
    position columns within the header.

    Parameters
    ----------
    header : `list` of `str`
        The header row to search.
    chr_name : `str`
        The name of the chromsomosome name column.
    start_pos : `str`
        The name of the start position column.
    end_pos : `str`
        The name of the end position column.

    Returns
    -------
    chr_name_idx : `int`
        The index of the chromosome name column.
    start_pos_idx : `int`
        The index of the start position column.
    end_pos_idx : `int`
        The index of the end position column.

    Raises
    ------
    ValueError
        If any of the chr_name, start_pos or end_pos columns can't be found.
    """
    end_pos_func = get_end_pos
    columns = []
    for i in [chr_name, start_pos]:
        columns.append(header.index(i))

    if end_pos is not None:
        columns.append(header.index(end_pos))
    elif ref_allele is not None:
        columns.append(header.index(ref_allele))
        end_pos_func = get_ref_end_pos
    else:
        raise IndexError("no end position column defined")

    return columns[0], columns[1], columns[2], end_pos_func


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_end_pos(chr_name, start_pos, end_pos):
    """Return the chromosome, start/end position that was given to the
    function.

    Essentially this acts as a pass through with positional int casting.

    Parameters
    ----------
    chr_name : `str`
        The chromosome name.
    start_pos : `str` or `int`
        The start position.
    end_pos : `str` or `int`
        The end position.

    Returns
    -------
    chr_name : `str`
        The chromosome name.
    start_pos : `int`
        The start position.
    end_pos : `int`
        The end position.
    """
    return chr_name, int(start_pos), int(end_pos)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def get_ref_end_pos(chr_name, start_pos, ref_allele):
    """Return the chromosome, start/end position, where the end position is
    calculated from the reference allele length.

    Parameters
    ----------
    chr_name : `str`
        The chromosome name.
    start_pos : `str` or `int`
        The start position.
    ref_allele : `str`
        The reference allele.

    Returns
    -------
    chr_name : `str`
        The chromosome name.
    start_pos : `int`
        The start position.
    end_pos : `int`
        The end position.
    """
    start_pos = int(start_pos)
    end_pos = start_pos + len(ref_allele) - 1
    return chr_name, start_pos, end_pos


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _split_ext(infile):
    """Split the infile into root file path and the extension.

    Try to recognised compressed extensions first before falling back to the
    basic Python file extension parser.

    Parameters
    ----------
    infile : `str`
        The input file name.

    Returns
    -------
    root_file : `str`
        The file name minus the extension.
    extension : `str`
        The file extension.
    """
    outer_ext = ['', 'b?gz', 'bgi']
    inner_ext = ['txt', 'csv', 'vcf', 'tab', 'bgen']

    # First try and be a bit smarter with the file extension extraction
    for o in outer_ext:
        for i in inner_ext:
            match = re.search(
                r'\.{0}{1}{2}$'.format(
                    i,
                    r'\.' * bool(len(outer_ext)),
                    o
                ),
                infile
            )

            try:
                root_name = infile[:match.start()]
                ext = infile[match.start():]
                return root_name, ext
            except AttributeError:
                pass

    # Fallback to the the basic python version
    return os.path.splitext(infile)


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _out_file_name(root_name, extension, region_idx, chr_name, start_pos,
                   end_pos, out_dir, out_ext, pheno_file, cov_file):
    """Generate an output file name from the input file components and the
    region number/positional information. No checks are done for spaces.

    Parameters
    ----------
    root_name : `str`
        The file name minus the extension.
    extension : `str`
        The file extension.
    region_idx : `int`
        The region number.
    chr_name : `str`
        The chromosome name of the region.
    start_pos : `int`
        The start position of the region.
    end_pos : `int`
        The end position of the region.

    Returns
    -------
    infile_name : `str`
        The input file name <root_name><extension>.
    outfile_name : `str`
        The output file name. This is the:
        <root_name>.<region_idx>.<chr_name>.<start_pos>-<end_pos>.<extension>
    """
    infile = os.path.realpath('{0}{1}'.format(root_name, extension)).encode()

    out_ext = out_ext or extension
    outfile = '{0}.{1}.{2}.{3}-{4}{5}'.format(
        root_name, region_idx, chr_name.decode(), start_pos,
        end_pos, out_ext
    ).encode()

    if out_dir is None:
        outfile = os.path.basename(outfile)
    elif out_dir != b'INFILE':
        outfile = os.path.join(out_dir, os.path.basename(outfile))

    return [infile, outfile, pheno_file, cov_file]


# ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
def _dummy(*args):
    """A dummy function, that returns an empty list.
    """
    return []


# @@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
if __name__ == '__main__':
    main()
